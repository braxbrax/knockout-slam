﻿using UnityEngine;
using System.Collections;

public class MainMenu : Menu
{
    public Texture splash;
    public Texture playButton;
    public Texture howButton;
	/// <summary>
	/// Displays all of the GUI components for this MainMenu.
	/// </summary>
	public override void Display()
	{
		// Main Menu Window
		GUI.BeginGroup(new Rect(Screen.width / 2.0f - 300.0f, Screen.height / 2.0f - 180.0f, 600.0f, 360.0f));
		
		//GUI.Box(new Rect(0.0f, 0.0f, 600.0f, 360.0f), splash);
        GUI.DrawTexture(new Rect(0.0f, 0.0f, 600.0f, 360.0f), splash, ScaleMode.ScaleToFit, true, 0);
		// Standard Mode Button
		if (GUI.Button(new Rect(60.0f, 150.0f, 210.0f, 150.0f), playButton))
		{
			GUIManager.Instance.CurrentMenu = GUIManager.Instance.playerSelectMenu;
		}

		if (GUI.Button(new Rect(330.0f, 150.0f, 210.0f, 150.0f), howButton))
		{
			GUIManager.Instance.CurrentMenu = GUIManager.Instance.instructionsMenu;
		}
		
		GUI.EndGroup();
	}
}