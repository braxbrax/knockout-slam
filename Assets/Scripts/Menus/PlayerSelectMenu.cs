﻿using UnityEngine;
using System.Collections;

public class PlayerSelectMenu : Menu
{
    public Texture selectMenu;
    public Texture ready;
    public Texture waiting;
    public Texture backButton;
    public Texture left;
    public Texture right;

    public Texture p1Tile;
    public Texture p2Tile;
    public Texture p3Tile;
    public Texture p4Tile;

    public Texture p1TileOff;
    public Texture p2TileOff;
    public Texture p3TileOff;
    public Texture p4TileOff;

	public bool p1Ready = false;
    public bool p2Ready = false;
    public bool p3Ready = false;
    public bool p4Ready = false;
    public bool readyToGo = false;

    public int stock = 3;

	void Update()
	{
		setter();
	}

    //takes a parameter representing the player number and toggles their readiness
    void toggleActive(int playerNumber)
    {
        if(p1Ready && playerNumber == 1)
        {
            p1Ready = false;
        }
        else if(!p1Ready && playerNumber == 1)
        {
            p1Ready = true;
        }

        if (p2Ready && playerNumber == 2)
        {
            p2Ready = false;
        }
        else if (!p2Ready && playerNumber == 2)
        {
            p2Ready = true;
        }

        if (p3Ready && playerNumber == 3)
        {
            p3Ready = false;
        }
        else if (!p3Ready && playerNumber == 3)
        {
            p3Ready = true;
        }

        if (p4Ready && playerNumber == 4)
        {
            p4Ready = false;
        }
        else if (!p4Ready && playerNumber == 4)
        {
            p4Ready = true;
        }
    }

    //called every display loop, should allow players to toggle readiness using the controller
    void setter()
    {
        //if a player presses action
        if (Input.GetButtonUp("Control" + GamePlayManager.Instance.players[0].controlIdx + "Dash"))
        {
            toggleActive(1);
        }
		if (Input.GetButtonUp("Control" + GamePlayManager.Instance.players[1].controlIdx + "Dash"))
        {
            toggleActive(2);
        }
		if (Input.GetButtonUp("Control" + GamePlayManager.Instance.players[2].controlIdx + "Dash"))
        {
            toggleActive(3);
        }
		if (Input.GetButtonUp("Control" + GamePlayManager.Instance.players[3].controlIdx + "Dash"))
        {
            toggleActive(4);
        }
    }

    
	/// <summary>
	/// Displays all of the GUI components for this PlayerSelectMenu.
	/// </summary>
	public override void Display()
	{
		// Player Selection Window
		GUI.BeginGroup(new Rect(Screen.width / 2.0f - 300.0f, Screen.height / 2.0f - 180.0f, 600.0f, 360.0f));

        //draw the background for the gui
        GUI.DrawTexture(new Rect(0.0f, 0.0f, 600.0f, 360.0f), selectMenu, ScaleMode.ScaleToFit, true, 0);

        int counter = 0;
        //draws each player color depending on readiness
        if (p1Ready)
        {
            counter++;
            if (GUI.Button(new Rect(60.0f, 60.0f, 240.0f, 120.0f), p1Tile))
            {
                p1Ready = false;
            }
        }
        else
        {
            if (GUI.Button(new Rect(60.0f, 60.0f, 240.0f, 120.0f), p1TileOff))
            {
                p1Ready = true;
            }
        }

        if (p2Ready)
        {
            counter++;
            if (GUI.Button(new Rect(300.0f, 60.0f, 240.0f, 120.0f), p2Tile))
            {
                p2Ready = false;
            }
        }
        else
        {
            if (GUI.Button(new Rect(300.0f, 60.0f, 240.0f, 120.0f), p2TileOff))
            {
                p2Ready = true;
            }
        }

        if (p3Ready)
        {
            counter++;
            if (GUI.Button(new Rect(60.0f, 180.0f, 240.0f, 120.0f), p3Tile))
            {
                p3Ready = false;
            }
        }
        else
        {
            if (GUI.Button(new Rect(60.0f, 180.0f, 240.0f, 120.0f), p3TileOff))
            {
                p3Ready = true;
            }
        }

        if (p4Ready)
        {
            counter++;
            if (GUI.Button(new Rect(300.0f, 180.0f, 240.0f, 120.0f), p4Tile))
            {
                p4Ready = false;
            }
        }
        else
        {
            if (GUI.Button(new Rect(300.0f, 180.0f, 240.0f, 120.0f), p4TileOff))
            {
                p4Ready = true;
            }
        }

        //determines condition of readyToGo
        if (counter >= 2)
        {
            readyToGo = true;
        }
        else
        {
            readyToGo = false;
        }
        counter = 0;

        //draws the button leading to the next screen if readyToGo
        if (readyToGo)
        {
            if (GUI.Button(new Rect(360.0f, 300.0f, 180.0f, 60.0f), ready))
            {
				GamePlayManager.Instance.players[0].IsUsed = p1Ready;
				GamePlayManager.Instance.players[1].IsUsed = p2Ready;
				GamePlayManager.Instance.players[2].IsUsed = p3Ready;
				GamePlayManager.Instance.players[3].IsUsed = p4Ready;

				GamePlayManager.Instance.startStock = stock;

                GUIManager.Instance.CurrentMenu = GUIManager.Instance.levelSelectMenu;
            }
        }
        else
        {
            GUI.DrawTexture(new Rect(360.0f, 300.0f, 180.0f, 60.0f), waiting, ScaleMode.ScaleToFit, true, 0);
        }

        //stock manipulation
        if (GUI.Button(new Rect(360.0f, 0.0f, 60.0f, 60.0f), left))
        {
            stock--;
            if(stock < 1)
            {
                stock = 1;
            }
        }
        if (GUI.Button(new Rect(480.0f, 0.0f, 60.0f, 60.0f), right))
        {
            stock++;
        }
        GUI.Box(new Rect(420, 0, 60, 60), "\n" + stock);

        // backButton
        if (GUI.Button(new Rect(60.0f, 300.0f, 120.0f, 60.0f), backButton))
        {
            GUIManager.Instance.CurrentMenu = GUIManager.Instance.mainMenu;
            p1Ready = false;
            p2Ready = false;
            p3Ready = false;
            p4Ready = false;
            readyToGo = false;
            stock = 3;
        }

        GUI.EndGroup();
	}
}