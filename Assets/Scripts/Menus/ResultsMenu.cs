﻿using UnityEngine;
using System.Collections;

public class ResultsMenu : Menu
{
    public Texture resultMenu;

    public Texture victory1;
    public Texture victory2;
    public Texture victory3;
    public Texture victory4;

    int scenario = 0;
	/// <summary>
	/// Displays all of the GUI components for this GameplayMenu.
	/// </summary>
    /// 

    //checks which of the competing players won
    public void setResult()
    {
        if (GamePlayManager.Instance.players[0].stock > 0 && GamePlayManager.Instance.players[0].IsUsed)
        {
            scenario = 1;
        }
        else if (GamePlayManager.Instance.players[1].stock > 0 && GamePlayManager.Instance.players[0].IsUsed)
        {
            scenario = 2;
        }
        else if (GamePlayManager.Instance.players[2].stock > 0 && GamePlayManager.Instance.players[0].IsUsed)
        {
            scenario = 3;
        }
        else
        {
            scenario = 4;
        }
    }

	public override void Display()
	{
        //check player stock to determine the winner
        setResult();

		// results Window
        GUI.BeginGroup(new Rect(Screen.width / 2.0f - 300.0f, Screen.height / 2.0f - 180.0f, 600.0f, 360.0f));

        //background
        GUI.DrawTexture(new Rect(0.0f, 0.0f, 600.0f, 360.0f), resultMenu, ScaleMode.ScaleToFit, true, 0);

        if(scenario == 1)
        {
            GUI.DrawTexture(new Rect(180.0f, 30.0f, 240.0f, 60.0f), victory1, ScaleMode.ScaleToFit, true, 0);
        }
        else if(scenario == 2)
        {
            GUI.DrawTexture(new Rect(180.0f, 30.0f, 240.0f, 60.0f), victory2, ScaleMode.ScaleToFit, true, 0);
        }
        else if (scenario == 3)
        {
            GUI.DrawTexture(new Rect(180.0f, 30.0f, 240.0f, 60.0f), victory3, ScaleMode.ScaleToFit, true, 0);
        }
        else if (scenario == 4)
        {
            GUI.DrawTexture(new Rect(180.0f, 30.0f, 240.0f, 60.0f), victory4, ScaleMode.ScaleToFit, true, 0);
        }


		if(GUI.Button(new Rect(60, 120, 210, 90), "Stage Select"))
		{
            //reset stock
            for (int i = 0; i < 4; i++ )
            {
                if (GamePlayManager.Instance.players[i].IsUsed)
                {
                    GamePlayManager.Instance.players[i].stock = 3;
                }
            }

            GUIManager.Instance.CurrentMenu = GUIManager.Instance.levelSelectMenu;
		}
		if(GUI.Button(new Rect(60, 240, 210,90), "Rematch"))
		{
            GUIManager.Instance.CurrentMenu = GUIManager.Instance.gameplayMenu;
		}
        if (GUI.Button(new Rect(300, 120, 210, 90), "Player Select"))
        {
            GUIManager.Instance.CurrentMenu = GUIManager.Instance.playerSelectMenu;
        }
        if (GUI.Button(new Rect(300, 240, 210, 90), "Main Menu"))
        {
            GUIManager.Instance.CurrentMenu = GUIManager.Instance.mainMenu;
        }
	
		GUI.EndGroup();
	}
}
