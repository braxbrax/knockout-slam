﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A GUI menu, screen, or HUD display.
/// </summary>
public abstract class Menu : MonoBehaviour
{


	/// <summary>
	/// Displays all of the GUI components for this Menu.
	/// </summary>
	public abstract void Display();
}