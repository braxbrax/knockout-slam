﻿using UnityEngine;
using System.Collections;

public class ModeSelectMenu : Menu
{
	/// <summary>
	/// Displays all of the GUI components for this ModeSelectMenu.
	/// </summary>
	public override void Display()
	{
		// Mode Selection Window
		GUI.BeginGroup(new Rect(Screen.width / 2.0f - 300.0f, Screen.height / 2.0f - 180.0f, 600.0f, 360.0f));

			GUI.Box(new Rect(0.0f, 0.0f, 600.0f, 360.0f), "| Select game mode |");

			// Standard Mode Button
			if (GUI.Button(new Rect(240.0f, 165.0f, 120.0f, 30.0f), "Standard"))
			{
				GUIManager.Instance.CurrentMenu = GUIManager.Instance.playerSelectMenu;
			}

		GUI.EndGroup();
	}
}