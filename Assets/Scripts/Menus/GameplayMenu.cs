using UnityEngine;
using System.Collections;

public class GameplayMenu : Menu
{
	/// <summary>
	/// Displays all of the GUI components for this GameplayMenu.
	/// </summary>
    /// 
    public Texture stockTile;
    public Texture redSplash;
    public Texture blueSplash;
    public Texture greenSplash;
    public Texture yellowSplash;

    public GUIStyle hudStyle;

    public void helpStock(float x, float y, int lives)
    {
        if(lives > 5)
        {
            GUI.DrawTexture(new Rect(x, y, 30.0f, 30.0f), stockTile, ScaleMode.ScaleToFit, true, 0);
            GUI.Label(new Rect(x + 25, y - 4, 100, 20), "x" + (lives), hudStyle);
        }
        else if (lives == 0)
        {
            GUI.Label(new Rect(x, y-4, 100, 20), "Knockout!", hudStyle);
        }
        else
        {
            for (int i = 1; i < lives + 1; i++)
            {
                GUI.DrawTexture(new Rect(x + (i * 30), y, 30.0f, 30.0f), stockTile, ScaleMode.ScaleToFit, true, 0);
            }
        }
    }

	public override void Display()
	{
		GamePlayManager.Instance.GameUpdate ();
		// gameplay Window
	
		//Player 1 info
		if (GamePlayManager.Instance.players [0].IsUsed) {
            GUI.DrawTexture(new Rect(0, 0, 300, 150), redSplash, ScaleMode.ScaleToFit, true, 0);
            GUI.Box(new Rect(0, 0, 300, 150), "Player 1\n\n" + "Charge: " + (int)(GamePlayManager.Instance.players[0].ChargeCount * 100) + "%", hudStyle);
            helpStock(50, 50, GamePlayManager.Instance.players[0].stock);
		}

		//Player 2 info
		if (GamePlayManager.Instance.players [1].IsUsed) {
            GUI.DrawTexture(new Rect(Screen.width - 300, 0, 300, 150), blueSplash, ScaleMode.ScaleToFit, true, 0);
            GUI.Box(new Rect(Screen.width - 300, 0, 300, 150), "Player 2\n\n" + "Charge: " + (int)(GamePlayManager.Instance.players[1].ChargeCount * 100) + "%", hudStyle);
            helpStock(Screen.width - 250, 50, GamePlayManager.Instance.players[1].stock);
		}

		//Player 3 info
		if (GamePlayManager.Instance.players [2].IsUsed) {
            GUI.DrawTexture(new Rect(0, Screen.height - 150, 300, 150), greenSplash, ScaleMode.ScaleToFit, true, 0);
            GUI.Box(new Rect(0, Screen.height - 150, 300, 150), "Player 3\n\n" + "Charge: " + (int)(GamePlayManager.Instance.players[2].ChargeCount * 100) + "%", hudStyle);
            helpStock(50, Screen.height - 100, GamePlayManager.Instance.players[2].stock);
		}

		//Player 4 info
		if (GamePlayManager.Instance.players [3].IsUsed) {
            GUI.DrawTexture(new Rect(Screen.width - 300, Screen.height - 150, 300, 150), yellowSplash, ScaleMode.ScaleToFit, true, 0);
            GUI.Box(new Rect(Screen.width - 300, Screen.height - 150, 300, 150), "Player 4\n\n" + "Charge: " + (int)(GamePlayManager.Instance.players[3].ChargeCount * 100) + "%", hudStyle);
            helpStock(Screen.width - 250, Screen.height - 100, GamePlayManager.Instance.players[3].stock);
		}
	}
}
