﻿using UnityEngine;
using System.Collections;

public class LevelSelectMenu : Menu
{
    public Texture stageMenu;
    public Texture backButton;
    public Texture discTile;
    public Texture donutTile;
    public Texture reticuleTile;
    public Texture swissTile;

	/// <summary>
	/// Displays all of the GUI components for this LevelSelectMenu.
	/// </summary>
	public override void Display()
	{
		// Level Selection Window
		GUI.BeginGroup(new Rect(Screen.width / 2.0f - 300.0f, Screen.height / 2.0f - 180.0f, 600.0f, 360.0f));

        GUI.DrawTexture(new Rect(0.0f, 0.0f, 600.0f, 360.0f), stageMenu, ScaleMode.ScaleToFit, true, 0);

		// Set Level

		if (GUI.Button(new Rect(60, 60, 225.0f, 105.0f), discTile))
		{
            GamePlayManager.Instance.levelLoad = 1;
			GUIManager.Instance.CurrentMenu = GUIManager.Instance.forcedInform;
		}
        if (GUI.Button(new Rect(315, 60, 225.0f, 105.0f), donutTile))
        {
            GamePlayManager.Instance.levelLoad = 2;
            GUIManager.Instance.CurrentMenu = GUIManager.Instance.forcedInform;
        }
        if (GUI.Button(new Rect(60, 195, 225.0f, 105.0f), reticuleTile))
        {
            GamePlayManager.Instance.levelLoad = 3;
            GUIManager.Instance.CurrentMenu = GUIManager.Instance.forcedInform;
        }
        if (GUI.Button(new Rect(315, 195, 225.0f, 105.0f), swissTile))
        {
            GamePlayManager.Instance.levelLoad = 4;
            GUIManager.Instance.CurrentMenu = GUIManager.Instance.forcedInform;
        }
        // backButton
        if (GUI.Button(new Rect(60.0f, 300.0f, 120.0f, 60.0f), backButton))
        {
            GUIManager.Instance.CurrentMenu = GUIManager.Instance.playerSelectMenu;
        }

		GUI.EndGroup();
	}
}