﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Stores all of the data for a specific level in the game.
/// </summary>
public class Level : MonoBehaviour
{
	public string title;
	public List<Transform> playerSpawnLocs;   // List of spawn locations for players
	public List<Transform> pickupSpawnLocs;   // List of spawn locations for pickups
}