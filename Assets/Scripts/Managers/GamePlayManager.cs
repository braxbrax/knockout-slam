﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GamePlayManager : Singleton<GamePlayManager>
{
	public List<Player> players;
	public GameObject level ;
    public Level currentLevel;
	public float matchTime;   // Duration of match
	public int startStock;
    public int levelLoad;   // let's forcedInform know which level to load

	public AudioClip menuMusic;

	//LEVELS
	public GameObject lvlDISK;
	public GameObject lvlDONUT;
	public GameObject lvlRECTICULE;
	public GameObject lvlSWISS;

	/// <summary>
	/// Protected constructor that can't be used, forcing this object to be a singleton.
	/// </summary>
	protected GamePlayManager() {}

	// Sets all values to default
	public void StartGame(GameObject lvl)
	{

		//create and save level gameobject
		level = (GameObject)Instantiate (lvl.gameObject);

		currentLevel = (Level)level.GetComponent<Level> ();

        for (int i = 0; i < players.Count; i++)
        {
            //checks first to see of there's actually a player for the character before spawning it
            if (players[i].IsUsed)
            {
                players[i].stock = startStock;
                SpawnPlayer(players[i], currentLevel.playerSpawnLocs[i]);
                players[i].gameObject.SetActive(true);
            }
        }
	}

	public void GameUpdate()
	{
		int count = 0;
		foreach(Player p in players)
		{
			if(p.IsUsed && p.stock != 0){
				count++;
			}
		}

		if (count <= 1) {
			EndGame();		
		}
	}
	 
	public void EndGame()
	{
		//hide level
		//level.SetActive (false);

        Destroy(level);

		foreach (Player p in players) {
			p.Reset();
			p.gameObject.SetActive(false);
		}

		GameManager.Instance.audio.clip=menuMusic;
		GameManager.Instance.audio.Play();

		GUIManager.Instance.CurrentMenu = GUIManager.Instance.resultsMenu;
	}

	public void SpawnPlayer(Player player)
	{
		//select random spawn point.
		//move player to spawn point

		int spawn = (int) Random.Range(0, currentLevel.playerSpawnLocs.Count);
		player.baseRigidbody.isKinematic = true;
		player.baseTransform.CopyNoScale(currentLevel.playerSpawnLocs[spawn]);
		player.baseRigidbody.isKinematic = false;
		player.topTransform.CopyNoScale(player.baseTransform);
	}

	public void SpawnPlayer(Player player, Transform spawnPoint)
	{
		//for spawning players to select location at game start/reset of game
		player.baseRigidbody.isKinematic = true;
		player.baseTransform.CopyNoScale(spawnPoint.transform);
		player.baseRigidbody.isKinematic = false;
		player.topTransform.CopyNoScale(player.baseTransform);
	}
}