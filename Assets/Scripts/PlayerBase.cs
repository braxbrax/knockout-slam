﻿using UnityEngine;
using System.Collections;

public class PlayerBase : MonoBehaviour 
{

	public delegate void BaseCollisionEnterEvent(Collision2D col);
	public event BaseCollisionEnterEvent BaseCollisionEnter;

	public delegate void BaseOnTriggerEnterEvent(Collider2D col);
	public BaseOnTriggerEnterEvent BaseOnTriggerEnter;

	public delegate void BaseOnTriggerExitEvent(Collider2D col);
	public BaseOnTriggerExitEvent BaseOnTriggerExit;

	public delegate void BaseOnCollisionExitEvent(Collision2D col);
	public BaseOnCollisionExitEvent BaseCollisionExit;

	void OnCollisionEnter2D(Collision2D col)
	{
			BaseCollisionEnter(col);
	}
	void OnCollisionExit2D(Collision2D col)
	{
		BaseCollisionExit(col);
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		BaseOnTriggerEnter(col);
	}
	void OnTriggerExit2D(Collider2D col)
	{
		BaseOnTriggerExit(col);
	}
}
