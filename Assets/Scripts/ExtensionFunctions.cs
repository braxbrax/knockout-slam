﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Contains all of the extension functions for the Transform class.
/// </summary>
public static class ExtensionFunctions
{
	/// <summary>
	/// Resets the global position, global rotation, and local scale of the transform to default values.
	/// Note: Global scale cannot be set directly very easily so local scale is set instead.
	/// </summary>
	/// <param name="transform">The Transform being reset.</param>
	public static void Reset(this Transform transform)
	{
		transform.position = Vector3.zero;
		transform.rotation = Quaternion.identity;
		transform.localScale = new Vector3(1f, 1f, 1f);
	}

	/// <summary>
	/// Resets the local position, rotation, and scale of the transform to default values.
	/// </summary>
	/// <param name="transform">The Transform being reset.</param>
	public static void ResetLocal(this Transform transform)
	{
		transform.localPosition = Vector3.zero;
		transform.localRotation = Quaternion.identity;
		transform.localScale = new Vector3(1f, 1f, 1f);
	}

	/// <summary>
	/// Copies the global position, global rotation, and local scale of the other transform into this transform.
	/// Note: Global scale cannot be set directly very easily so local scale is set instead.
	/// </summary>
	/// <param name="transform">The Transform to be copied into.</param>
	/// <param name="other">The Transform to be copied from.</param>
	public static void Copy(this Transform transform, Transform other)
	{
		transform.position = other.position;
		transform.rotation = other.rotation;
		transform.localScale = other.localScale;
	}

	/// <summary>
	/// Copies the local position, rotation, and scale of the other transform into this transform.
	/// </summary>
	/// <param name="transform">The Transform to be copied into.</param>
	/// <param name="other">The Transform to be copied from.</param>
	public static void CopyLocal(this Transform transform, Transform other)
	{
		transform.localPosition = other.localPosition;
		transform.localRotation = other.localRotation;
		transform.localScale = other.localScale;
	}

	/// <summary>
	/// Copies the global position and rotation of the other transform into this transform.
	/// </summary>
	/// <param name="transform">The Transform to be copied into.</param>
	/// <param name="other">The Transform to be copied from.</param>
	public static void CopyNoScale(this Transform transform, Transform other)
	{
		transform.position = other.position;
		transform.rotation = other.rotation;
	}

	/// <summary>
	/// Copies the local position and rotation of the other transform into this transform.
	/// </summary>
	/// <param name="transform">The Transform to be copied into.</param>
	/// <param name="other">The Transform to be copied from.</param>
	public static void CopyLocalNoScale(this Transform transform, Transform other)
	{
		transform.localPosition = other.localPosition;
		transform.localRotation = other.localRotation;
	}
}