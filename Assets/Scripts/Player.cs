﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
	public bool IsUsed = false;
	public int controlIdx;
	public Rigidbody2D baseRigidbody;
	public Transform baseTransform;
	public Transform topTransform;
	public float chargeRate;
	public float chargeMultiplyer;
	public float speed;
	public int stock;

	private Vector2 directionalInput;
	private Vector2 currentDirection;
	private Vector2 movement;
	private float chargeCount;
	private float moveX;
	private float moveY;
	private bool charging;
	private bool respawning;
    private int platforms;

	public float ChargeCount
	{
		get{return chargeCount;}
	}

	public AudioClip collideSound;
	public AudioClip chargeSound;

	void Start()
	{
		baseRigidbody.gameObject.GetComponent<PlayerBase>().BaseCollisionEnter += OnBaseCollisionEnter;
		baseRigidbody.gameObject.GetComponent<PlayerBase>().BaseOnTriggerExit += BaseOnTriggerExit;
		baseRigidbody.gameObject.GetComponent<PlayerBase>().BaseCollisionExit += OnBaseCollisionExit;
		baseRigidbody.gameObject.GetComponent<PlayerBase>().BaseOnTriggerEnter += BaseOnTriggerEnter;

		init ();
	}

	public void init(){
		currentDirection = new Vector2(1f, 1f);
		respawning = false;
	}

	void Update()
	{

		if(!respawning)
		{
			directionalInput = new Vector2(Input.GetAxis("Control" + controlIdx + "XAxis"), Input.GetAxis("Control" + controlIdx + "YAxis"));
			charging = Charge();

			if (directionalInput.sqrMagnitude != 0f)
			{
				topTransform.eulerAngles = new Vector3(topTransform.eulerAngles.x, topTransform.eulerAngles.y, (Mathf.Atan2(directionalInput.y, directionalInput.x) - (Mathf.PI / 2f)) * Mathf.Rad2Deg);
			}
			topTransform.position = baseTransform.position;
		}
	}

	void FixedUpdate()
	{
		if(respawning)
		{
			baseRigidbody.velocity = Vector2.zero;
		}

		if(!charging)
		{
			//if the player isnt charging, add forces as normal from joystick input
			movement = new Vector2(directionalInput.x * speed, directionalInput.y * speed);
			baseRigidbody.AddForce(movement);
			chargeCount = 0;
		}

		if(directionalInput !=Vector2.zero)
		{
			currentDirection = directionalInput;
		}

		if(baseRigidbody.velocity.magnitude > 2)
		{
			//if the player (after a dash) is going faster than the normal movement speed, increase friction
			baseRigidbody.velocity = baseRigidbody.velocity * .96f;
		}

		baseRigidbody.velocity = Vector2.ClampMagnitude(baseRigidbody.velocity, 1000f);
	}

	private bool Charge()
	{
		if (Input.GetButton("Control" + controlIdx + "Dash"))
		{
			audio.PlayOneShot(chargeSound,0.9f);

			if(chargeCount <= 1)
			{
				chargeCount += chargeRate;

				if(chargeCount > 1)
				{
					chargeCount = 1;
				}
			}

			return true;
		}
		else if(Input.GetButtonUp("Control" + controlIdx + "Dash"))
		{
			float dashSpeed = chargeCount * chargeMultiplyer;
			currentDirection.Normalize();
			Vector2 dash = new Vector2(currentDirection.x * dashSpeed, currentDirection.y * dashSpeed);
			baseRigidbody.AddForce(dash);

			audio.Stop();
		}

		return false;
	}

	void OnBaseCollisionEnter(Collision2D col)
	{
		audio.PlayOneShot(collideSound, 0.6f);
	}

	void OnBaseCollisionExit(Collision2D col)
	{

	}

	void BaseOnTriggerEnter(Collider2D col)
	{

	}

	void BaseOnTriggerExit(Collider2D col)
	{
		if(col.tag.Equals("Platform"))
		{
			if(stock > 1)
			{
				StartCoroutine("Respawn");
			}
			else
			{
				this.gameObject.SetActive(false);
			}
			stock--;
		}
	}

	IEnumerator Respawn()
	{
		audio.Stop();

		topTransform.renderer.enabled = false;
		respawning = true;

		yield return new WaitForSeconds(3f);
		GamePlayManager.Instance.SpawnPlayer(this);


		respawning = false;
		topTransform.renderer.enabled = true;
	}

	public void Reset()
	{
		chargeCount = 0;
		charging = false;
		platforms = 0;
		topTransform.transform.rotation = Quaternion.identity;
	}
}